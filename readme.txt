*****Music Folder*****
Currently set to /home/pi/music/

*****Music File Structure*****
Song_Name_-_Artist_Name.mp3

*****Playlist File*****
Currently set to /home/pi/music/playlist/.playlist

*****Now Playing Feature*****
Add to synchronized_lights.py:

    # Output a bit about what we're about to play to the logs
    song_filename = os.path.abspath(song_filename)
    f = open(cm.HOME_DIR + '/py/nowplaying.txt','w+')
    f.write(song_filename) # python will convert \n to os.linesep
    f.close()

*****Troubleshooting*****
If something is not working, check file permissions first.


*****Contact Info*****
alex@alexmourer.com